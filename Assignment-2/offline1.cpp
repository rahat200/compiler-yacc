#include<iostream>
#include<cstring>
#include<vector>
#include<fstream>
using namespace std;
int hash(string key);
int SIZE = 20;

class SymbolInfo
{
public:
    string name;
    string type;
    SymbolInfo(string name,string type)
    {
        this->name = name;
        this->type = type;
    }
};

class SymbolTable
{
    public:
    vector <SymbolInfo> table[20];
    void insert(SymbolInfo symbol);
    bool lookup(SymbolInfo symbol);
    void dump();
};

void SymbolTable::insert(SymbolInfo symbol)
{
    if(lookup(symbol)==false)
    {
        int hashed_index = hash(symbol.name);
        table[hashed_index].push_back(symbol);
    }
    else
        cout<<"symbol already exists\n";
}

bool SymbolTable::lookup(SymbolInfo symbol)
{
    int i;
    int hashed_index = hash(symbol.name);
    if(!table[hashed_index].empty())
    {
        for(i=0;i<table[hashed_index].size();i++)
        {
            if(table[hashed_index][i].name == symbol.name && table[hashed_index][i].type == symbol.type)
            return true;
        }
    }

    return false;
}

void SymbolTable::dump()
{
    for(int i = 0; i<SIZE ;i++)
    {
        for(int j = 0;j<table[i].size();j++)
        {
            cout<<table[i][j].name<<"-->"<<table[i][j].type<<"\n";
        }
    }
}

int hash(string key)
{
    int i, sum = 0;
    for(i=0;i<key.size();i++)
    {
        sum+=key[i];
    }
    return (sum%SIZE);
}

int main()
{
    SymbolTable table;
    string line,key,type;
    int delim;
    ifstream fin("input.txt");
    while(getline(fin,line))
    {
        delim = line.find(",");
        key = line.substr(0,delim);
        type = line.substr(delim+2);
        //cout<<key<<"-"<<type<<"\n";
        SymbolInfo obj1(key,type);
        table.insert(obj1);
    }

    table.dump();
}


