%{

#include<iostream>
#include<cstring>
#include<vector>
#include<fstream>
#define SIZE 50
using namespace std;
int hash(string key);


class SymbolInfo
{
public:
    string name;
    string type;
    SymbolInfo(string name,string type)
    {
        this->name = name;
        this->type = type;
    }
};

class SymbolTable
{
    public:
    vector <SymbolInfo> table[SIZE];
    void insert(string key,string type);
    bool lookup(SymbolInfo symbol);
    void dump();
};

void SymbolTable::insert(string key,string type)
{
    
    SymbolInfo symbol(key,type);
    if(lookup(symbol)==false)
    {
        int hashed_index = hash(symbol.name);
        table[hashed_index].push_back(symbol);
    }
    else
        cout<<"\nsymbol already exists\n";
}

bool SymbolTable::lookup(SymbolInfo symbol)
{
    int i;
    int hashed_index = hash(symbol.name);
    if(!table[hashed_index].empty())
    {
        for(i=0;i<table[hashed_index].size();i++)
        {
            if(table[hashed_index][i].name == symbol.name )
            return true;
        }
    }

    return false;
}

void SymbolTable::dump()
{
    for(int i = 0; i<SIZE ;i++)
    {
        for(int j = 0;j<table[i].size();j++)
        {
            cout<<table[i][j].name<<"  -->"<<table[i][j].type<<"\n";
        }
    }
}

int hash(string key)
{
    int i, sum = 0;
    for(i=0;i<key.size();i++)
    {
        sum+=key[i];
    }
    return (sum%SIZE);
}


int lineCount = 0;
SymbolTable symTable;

%}

delim 			[ \t]
newline			\n

digit			[0-9]
digits			{digit}+
number 			{digits}(.{digits})?(E[+-]?{digits})?

letter          	[A-Za-z]
id              	({letter}|_)({letter}|{digit})*

relop			"<>"|"<="|">="|[=<>]
DOTDOT			"["|"]"|"("|")"|".."
COMMA	 		,

comments		"{"[^}]*
multiline_comment	"/*"([^*]|"*"[^/])*"*/"

str			\"[^"]*\"

%%

{str}		{symTable.insert(yytext,"STRING"); }

{comments}	{printf("found a comment %s",yytext);}
{multiline_comment}	{printf("found a multiline comment %s",yytext);}


"program"|"if"|"not"|"end"|"begin"|"else"|"then"|"do"|"while"|"function"|"Procedure"|"integer"|"real"|"var"|"oh"|"array"|"write"			{		printf("\nkeyword: %s at line : %d\n", yytext,lineCount);}

#include		{symTable.insert(yytext,"INCLUDE");}
{id}.h		{symTable.insert(yytext,"HEADER");}
{newline}    	{lineCount++;}

{delim}+	{printf("\nfound a\tdelim"); }

{number}	{symTable.insert(yytext,"NUMBER");}

{id}		{symTable.insert(yytext,"IDENTIFIER");}

"+"		{symTable.insert(yytext,"ADDOP");}
"-"		{symTable.insert(yytext,"SUBOP");}
"*"		{symTable.insert(yytext,"MULOP");}
"/"		{symTable.insert(yytext,"DIVOP");}

{relop}		{symTable.insert(yytext,"RELOP");}

":="		{symTable.insert(yytext,"ASSIGNOP");}

{DOTDOT}	{symTable.insert(yytext,"DOTDOT");}

{COMMA}		{symTable.insert(yytext,"COMMA");}
":"		{symTable.insert(yytext,"COLON");}
";"		{symTable.insert(yytext,"SEMICOLON");}

%%
int main()
{
	
	
	
	yylex();
	
	printf("\n\n\ndisplaying Symbol table entries\n");
	symTable.dump();
	
	return 0;
}
