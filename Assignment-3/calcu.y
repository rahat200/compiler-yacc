%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define YYSTYPE double
extern yylex();
void yyerror(char *);
int new_line = 0;

%}
%token PLUS MINUS NUMBER NEWLINE MUL DIV DOUBLESTERIK INC DEC MOD LS RS SIN COS TAN LBRACE RBRACE

%left PLUS MINUS
%left MUL DIV MOD

%left UMINUS
%left UPLUS

%%


input:
	 | input line
	 ;
 
 line: NEWLINE
	 | expr NEWLINE {if(!new_line)
	 					printf("\t%.10g\n",$1); 
	 				else 
	 				new_line=0;
	 				}
	 ;
 expr: term
 	 |expr PLUS expr	{$$ = $1 + $3;}
	 | expr MINUS expr	{$$ = $1 - $3;}
	 | expr MUL expr 	{$$ = $1 * $3;}
	 | expr DIV expr	{ if($3 == 0)
	 						{
	 							yyerror("cannot be divided by 0");
	 							new_line = 1;
	 						}
	 					  else {
	 					 	$$ = $1/$3;
	 					  }
	 					
	 					}
	 | expr MOD expr	{ if(round($1)!=$1 || round($3)!=$3) {
	 							yyerror("type must be integer");
	 							new_line = 1;
	 						}
	 						else 
	 							$$ = (int)$1 % (int)$3;
						 }

	 | expr LS term		{$$ = (int)$1 << (int)$3;}
	 | expr RS term		{$$ = (int)$1 >> (int)$3;}
	 | expr DOUBLESTERIK term	{$$ = pow($1,$3);}
	 | SIN term {$$ = sin(3.1416/180.0*$2);}
	 | COS term {$$ = cos(3.1416/180.0*$2);}
	 | TAN term {$$ = tan(3.1416/180.0*$2);}
	 | INC term		{$$ = $2 + 1;}
	 | term	INC		{$$ = $1 + 1;}
	 | DEC term		{$$ = $2 - 1;}
	 | term DEC		{$$ = $1 - 1;}
	 | MINUS term %prec UMINUS 		{$$ = -$2;}
	 | PLUS term %prec UPLUS 		{$$ = $2;} 
	 ;
 
 term: 
	 | factor
	 ;
 factor:
	 | NUMBER
	 | LBRACE expr RBRACE {$$=$2;} 
	 
	 ;

 
   
 %%
 void yyerror(char *s)
 {fprintf(stderr,"%s\n",s);}
 main()
 {
 yyparse();
 exit(0);
 }