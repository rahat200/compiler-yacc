%{
#include <malloc.h>
#include <stdlib.h>
void yyerror(char *);
#include "y.tab.h"
%}

WS [ \t]+
DIGIT [0-9]
NUMBER [-]?{DIGIT}+(\.{DIGIT}+)?
%%



{NUMBER} {sscanf(yytext,"%lf",&yylval); return NUMBER;}
"+" { return PLUS;}
"-" { return MINUS;}
"*" {return MUL;}
"/" {return DIV;}
"**" {return DOUBLESTERIK;}
"++" {return INC;}
"--" {return DEC;}
"\n" { return NEWLINE;}
"%"  {return MOD;}
"<<" {return LS;}
">>" {return RS;}
"sin" {return SIN;}
"cos" {return COS;}
"tan" {return TAN;}
"(" {return LBRACE;}
")" {return RBRACE;}



{WS} {}
%%



int yywrap(void){
return 1;}
