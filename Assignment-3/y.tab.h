
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     PLUS = 258,
     MINUS = 259,
     NUMBER = 260,
     NEWLINE = 261,
     MUL = 262,
     DIV = 263,
     DOUBLESTERIK = 264,
     INC = 265,
     DEC = 266,
     MOD = 267,
     LS = 268,
     RS = 269,
     SIN = 270,
     COS = 271,
     TAN = 272,
     LBRACE = 273,
     RBRACE = 274,
     UMINUS = 275,
     UPLUS = 276
   };
#endif
/* Tokens.  */
#define PLUS 258
#define MINUS 259
#define NUMBER 260
#define NEWLINE 261
#define MUL 262
#define DIV 263
#define DOUBLESTERIK 264
#define INC 265
#define DEC 266
#define MOD 267
#define LS 268
#define RS 269
#define SIN 270
#define COS 271
#define TAN 272
#define LBRACE 273
#define RBRACE 274
#define UMINUS 275
#define UPLUS 276




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


