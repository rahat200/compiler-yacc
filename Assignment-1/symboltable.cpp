#include<iostream>
#include<cstdio>
#include<string>
#include<fstream>
#define MAX 20
using namespace std;
int hashing(string str)
{
    unsigned long hash = 5381;
    int l=str.length();
    for (int i=0; i<l; i++)
    {
        hash = ((hash << 5) + hash) + str[i]; /* hash * 33 + c */
    }
    return hash%MAX;
}

struct SymbolInfo
{
    string symbol;
    string type;
    SymbolInfo * next;
    SymbolInfo();
};

class SymbolTable
{
    SymbolInfo *hash[MAX];
public:
    SymbolTable();
    ~SymbolTable();
    void insert();
    bool lookup();
    void dump();
};

SymbolInfo::SymbolInfo()
{
    next=NULL;
}

SymbolTable::SymbolTable()
{
    for (int i=0; i<MAX; i++)
    {
        hash[i]=NULL;
    }
}

SymbolTable::~SymbolTable()
{
    for (int i=0; i<MAX; i++)
    {
        SymbolInfo *t1,*t2;
        for(t1=hash[i]; t1; t1=t2)
        {
            t2=t1->next;
            delete t1;
        }
    }
}

void SymbolTable::insert()
{
    int x;
    string in,st1,st2;
    getline(cin,in);
    x=in.find(",");
    st1=in.substr(0,x);
    st2=in.substr(x+2);
    x=hashing(st1);
    if (!(hash[x]))
    {
        hash[x]=new SymbolInfo;
        hash[x]->symbol=st1;
        hash[x]->type=st2;
        hash[x]->next=NULL;
    }
    else
    {
        SymbolInfo *temp;
        temp=hash[x];
        hash[x]=new SymbolInfo;
        hash[x]->symbol=st1;
        hash[x]->type=st2;
        hash[x]->next=temp;
    }
}

bool SymbolTable::lookup()
{
    string sym;
    int x;
    cin>>sym;
    x=hashing(sym);
    for(SymbolInfo *temp=hash[x];temp!=NULL;temp=temp->next)
    {
        if (temp->symbol==sym) return true;
    }
    return false;
}

void SymbolTable::dump()
{
    for (int i=0; i<MAX; i++)
    {
        if (hash[i])
        {
            for (SymbolInfo *temp=hash[i];temp;temp=temp->next)
            {
                cout << temp->symbol << ", " << temp->type<<endl;
            }
        }
    }
}

int main()
{
    int x;
    ifstream ifs("in.txt");
    cin.rdbuf(ifs.rdbuf());
    bool flag=false;
    SymbolTable A;
    A.insert();
    A.insert();
    A.insert();
    A.insert();
    flag=A.lookup();
    if (flag) cout <<"Found."<<endl;
    else cout << "Not found."<<endl;
    A.dump();
    return 0;
}
